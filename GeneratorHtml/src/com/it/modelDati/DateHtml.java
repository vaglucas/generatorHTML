/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.modelDati;

/**
 *
 * @author Gomes
 */
public class DateHtml {

    private String _name_data_model_;
    private String _for_data_model_;
    private String _id_data_model_;
    private String _label_data_model_;

    public String getLabel_data_model_() {
        return _label_data_model_;
    }

    public void setLabel_data_model_(String _label_data_model_) {
        this._label_data_model_ = _label_data_model_;
    }
    public String getName_data_model_() {
        return _name_data_model_;
    }

    public void setName_data_model_(String _name_data_model_) {
        this._name_data_model_ = _name_data_model_;
    }

    public String getFor_data_model_() {
        return _for_data_model_;
    }

    public void setFor_data_model_(String _for_data_model_) {
        this._for_data_model_ = _for_data_model_;
    }

    public String getId_data_model_() {
        return _id_data_model_;
    }

    public void setId_data_model_(String _id_data_model_) {
        this._id_data_model_ = _id_data_model_;
    }

    public DateHtml() {
    }
    
    
}
