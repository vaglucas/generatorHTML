/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.modelDati;

/**
 *
 * @author Gomes
 */
public class Hidden {
    
    private String _name_model_input_;
    private String _id_model_hidden_;
    private String _class_model_hidden_;

    public Hidden() {
    }

    public String getName_model_input_() {
        return _name_model_input_;
    }

    public void setName_model_input_(String _name_model_input_) {
        this._name_model_input_ = _name_model_input_;
    }

    public String getId_model_hidden_() {
        return _id_model_hidden_;
    }

    public void setId_model_hidden_(String _id_model_hidden_) {
        this._id_model_hidden_ = _id_model_hidden_;
    }

    public String getClass_model_hidden_() {
        return _class_model_hidden_;
    }

    public void setClass_model_hidden_(String _class_model_hidden_) {
        this._class_model_hidden_ = _class_model_hidden_;
    }
    
    
}
