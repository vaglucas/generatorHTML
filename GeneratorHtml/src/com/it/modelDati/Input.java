/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.modelDati;

/**
 *
 * @author Gomes
 */
public class Input {
  private String _id_model_input_;
  private String _id_label_input;
  private String _name_model_input_;
  private String _desc_label_input_;
  private String _class_model_input_;
  private String classInputModel;

    public Input() {
    }

    public Input(String _id_model_input_, String _id_label_input, String _name_model_input_, String _desc_label_input_, String _class_model_input_, String classInputModel) {
        this._id_model_input_ = _id_model_input_;
        this._id_label_input = _id_label_input;
        this._name_model_input_ = _name_model_input_;
        this._desc_label_input_ = _desc_label_input_;
        this._class_model_input_ = _class_model_input_;
        this.classInputModel = classInputModel;
    }
  
  

    public String getId_model_input_() {
        return _id_model_input_;
    }

    public void setId_model_input_(String _id_model_input_) {
        this._id_model_input_ = _id_model_input_;
    }

    public String getId_label_input() {
        return _id_label_input;
    }

    public void setId_label_input(String _id_label_input) {
        this._id_label_input = _id_label_input;
    }

    public String getName_model_input_() {
        return _name_model_input_;
    }

    public void setName_model_input_(String _name_model_input_) {
        this._name_model_input_ = _name_model_input_;
    }

    public String getDesc_label_input_() {
        return _desc_label_input_;
    }

    public void setDesc_label_input_(String _desc_label_input_) {
        this._desc_label_input_ = _desc_label_input_;
    }

    public String getClass_model_input_() {
        return _class_model_input_;
    }

    public void setClass_model_input_(String _class_model_input_) {
        this._class_model_input_ = _class_model_input_;
    }

    public String getClassInputModel() {
        return classInputModel;
    }

    public void setClassInputModel(String classInputModel) {
        this.classInputModel = classInputModel;
    }
 
  
  
	@Override
	public String toString() {
		return "Input [_id_model_input_=" + _id_model_input_ + ", _name_model_input_=" + _name_model_input_ + ", _desc_label_input_=" + _desc_label_input_ + "]";
	}


}
