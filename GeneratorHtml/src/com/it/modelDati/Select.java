/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.modelDati;

/**
 *
 * @author Gomes
 */
public class Select {

    private String _class_select_;
    private String _name_select_;
    private String _id_select_;
    private String _classSelectModel_;
    private String _label_Select_model_;

    public String getClass_select_() {
        return _class_select_;
    }

    public void setClass_select_(String _class_select_) {
        this._class_select_ = _class_select_;
    }

    public String getName_select_() {
        return _name_select_;
    }

    public void setName_select_(String _name_select_) {
        this._name_select_ = _name_select_;
    }

    public String getId_select_() {
        return _id_select_;
    }

    public void setId_select_(String _id_select_) {
        this._id_select_ = _id_select_;
    }

    public String getClassSelectModel_() {
        return _classSelectModel_;
    }

    public void setClassSelectModel_(String _classSelectModel_) {
        this._classSelectModel_ = _classSelectModel_;
    }

    public String getLabel_Select_model_() {
        return _label_Select_model_;
    }

    public void setLabel_Select_model_(String _label_Select_model_) {
        this._label_Select_model_ = _label_Select_model_;
    }

    public Select() {
    }
    
    
}
