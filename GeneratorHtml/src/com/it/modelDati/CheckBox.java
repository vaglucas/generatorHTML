/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.modelDati;

/**
 *
 * @author Gomes
 */
public class CheckBox {

    private String _labelCheckboxModel_;
    private String _classCheckboxModel_;
    private String _classInputCheckBoxModel_;
    private String _value_checkbox_;
    private String _name_Checkbox_;
    private String _id_checkBox_;

    public String getLabelCheckboxModel_() {
        return _labelCheckboxModel_;
    }

    public void setLabelCheckboxModel_(String _labelCheckboxModel_) {
        this._labelCheckboxModel_ = _labelCheckboxModel_;
    }

    public String getClassCheckboxModel_() {
        return _classCheckboxModel_;
    }

    public void setClassCheckboxModel_(String _classCheckboxModel_) {
        this._classCheckboxModel_ = _classCheckboxModel_;
    }

    public String getClassInputCheckBoxModel_() {
        return _classInputCheckBoxModel_;
    }

    public void setClassInputCheckBoxModel_(String _classInputCheckBoxModel_) {
        this._classInputCheckBoxModel_ = _classInputCheckBoxModel_;
    }

    public String getValue_checkbox_() {
        return _value_checkbox_;
    }

    public void setValue_checkbox_(String _value_checkbox_) {
        this._value_checkbox_ = _value_checkbox_;
    }

    public String getName_Checkbox_() {
        return _name_Checkbox_;
    }

    public void setName_Checkbox_(String _name_Checkbox_) {
        this._name_Checkbox_ = _name_Checkbox_;
    }

    public String getId_checkBox_() {
        return _id_checkBox_;
    }

    public void setId_checkBox_(String _id_checkBox_) {
        this._id_checkBox_ = _id_checkBox_;
    }

    public CheckBox() {
    }

    public CheckBox(String _labelCheckboxModel_, String _classCheckboxModel_, String _classInputCheckBoxModel_, String _value_checkbox_, String _name_Checkbox_, String _id_checkBox_) {
        this._labelCheckboxModel_ = _labelCheckboxModel_;
        this._classCheckboxModel_ = _classCheckboxModel_;
        this._classInputCheckBoxModel_ = _classInputCheckBoxModel_;
        this._value_checkbox_ = _value_checkbox_;
        this._name_Checkbox_ = _name_Checkbox_;
        this._id_checkBox_ = _id_checkBox_;
    }
    
    
}
