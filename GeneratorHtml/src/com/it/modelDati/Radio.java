/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.it.modelDati;

/**
 *
 * @author Gomes
 */
public class Radio {

    private String _classModelRadio_;
    private String _labelModelRadio_;
    
    private String _name_radio_a_;
    private String _value_radio_a_;
    private String _id_radio_a_;
    private String _for_radio_a_; //for uguale al id
    private String _error_name_radio_a_;
    private String _desc_radio_a_;

    private String _name_radio_b_;
    private String _value_radio_b_;
    private String _for_radio_b_;
    private String _id_radio_b_;
    private String _error_name_radio_b_;
    private String _desc_radio_b_;
    
    
    public String getId_radio_a_() {
        return _id_radio_a_;
    }

    public void setId_radio_a_(String _id_radio_a_) {
        this._id_radio_a_ = _id_radio_a_;
    }

    public String getId_radio_b_() {
        return _id_radio_b_;
    }

    public void setId_radio_b_(String _id_radio_b_) {
        this._id_radio_b_ = _id_radio_b_;
    } 

    public String getClassModelRadio_() {
        return _classModelRadio_;
    }

    public void setClassModelRadio_(String _classModelRadio_) {
        this._classModelRadio_ = _classModelRadio_;
    }

    public String getLabelModelRadio_() {
        return _labelModelRadio_;
    }

    public void setLabelModelRadio_(String _labelModelRadio_) {
        this._labelModelRadio_ = _labelModelRadio_;
    }

    public String getName_radio_a_() {
        return _name_radio_a_;
    }

    public void setName_radio_a_(String _name_radio_a_) {
        this._name_radio_a_ = _name_radio_a_;
    }

    public String getValue_radio_a_() {
        return _value_radio_a_;
    }

    public void setValue_radio_a_(String _value_radio_a_) {
        this._value_radio_a_ = _value_radio_a_;
    }

    public String getFor_radio_a_() {
        return _for_radio_a_;
    }

    public void setFor_radio_a_(String _for_radio_a) {
        this._for_radio_a_= _for_radio_a;
    }

    public String getError_name_radio_a_() {
        return _error_name_radio_a_;
    }

    public void setError_name_radio_a_(String _error_name_radio_a_) {
        this._error_name_radio_a_ = _error_name_radio_a_;
    }

    public String getDesc_radio_a_() {
        return _desc_radio_a_;
    }

    public void setDesc_radio_a_(String _desc_radio_a_) {
        this._desc_radio_a_ = _desc_radio_a_;
    }

    public String getName_radio_b_() {
        return _name_radio_b_;
    }

    public void setName_radio_b_(String _name_radio_b_) {
        this._name_radio_b_ = _name_radio_b_;
    }

    public String getValue_radio_b_() {
        return _value_radio_b_;
    }

    public void setValue_radio_b_(String _value_radio_b_) {
        this._value_radio_b_ = _value_radio_b_;
    }

    public String getFor_radio_b_() {
        return _for_radio_b_;
    }

    public void setFor_radio_b_(String _for_radio_b_) {
        this._for_radio_b_ = _for_radio_b_;
    }

    public String getError_name_radio_b_() {
        return _error_name_radio_b_;
    }

    public void setError_name_radio_b_(String _error_name_radio_b_) {
        this._error_name_radio_b_ = _error_name_radio_b_;
    }

    public String getDesc_radio_b_() {
        return _desc_radio_b_;
    }

    public void setDesc_radio_b_(String _desc_radio_b_) {
        this._desc_radio_b_ = _desc_radio_b_;
    }

    public Radio() {
    }

    public Radio(String _classModelRadio_, String _labelModelRadio_, String _name_radio_a_, String _value_radio_a_, String _id_radio_a_, String _for_radio_a_, String _error_name_radio_a_, String _desc_radio_a_, String _name_radio_b_, String _value_radio_b_, String _for_radio_b_, String _id_radio_b_, String _error_name_radio_b_) {
        this._classModelRadio_ = _classModelRadio_;
        this._labelModelRadio_ = _labelModelRadio_;
        this._name_radio_a_ = _name_radio_a_;
        this._value_radio_a_ = _value_radio_a_;
        this._id_radio_a_ = _id_radio_a_;
        this._for_radio_a_ = _for_radio_a_;
        this._error_name_radio_a_ = _error_name_radio_a_;
        this._desc_radio_a_ = _desc_radio_a_;
        this._name_radio_b_ = _name_radio_b_;
        this._value_radio_b_ = _value_radio_b_;
        this._for_radio_b_ = _for_radio_b_;
        this._id_radio_b_ = _id_radio_b_;
        this._error_name_radio_b_ = _error_name_radio_b_;
    }

   
    
    
    
    
}
