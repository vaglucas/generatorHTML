package com.it.generator;

import com.it.modelDati.CheckBox;
import com.it.modelDati.DateHtml;
import com.it.modelDati.Hidden;
import com.it.modelDati.Input;
import com.it.modelDati.Radio;
import com.it.modelDati.Select;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InputGenerator {

    public String getInput(Input i) throws FileNotFoundException, IOException {
        StringBuilder contentBuilder = new StringBuilder();
        InputStream stream = this.getClass().getResourceAsStream("/com/it/html/modelInput.html");
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String str;
        while ((str = in.readLine()) != null) {
            str = str.replaceAll("_id_label_input_", i.getId_label_input());
            str = str.replaceAll("_class_model_input_", i.getClass_model_input_());
            str = str.replaceAll("_name_model_input_", i.getName_model_input_());
            str = str.replaceAll("_id_model_input_", i.getId_model_input_());
            str = str.replaceAll("classInputModel", i.getClassInputModel());
            str = str.replaceAll("_desc_label_input_", i.getDesc_label_input_());
            contentBuilder.append(str + "\n");
        }
        in.close();
        String content = contentBuilder.toString();
        System.out.println(content);
        return content;
    }

    public String getHidden(Hidden i) throws FileNotFoundException, IOException {
        StringBuilder contentBuilder = new StringBuilder();
        InputStream stream = this.getClass().getResourceAsStream("/com/it/html/hidenModel.html");
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String str;
        while ((str = in.readLine()) != null) {
            str = str.replaceAll("_id_label_input_", i.getId_model_hidden_());
            str = str.replaceAll("_name_model_input_", i.getName_model_input_());
            str = str.replaceAll("_class_model_hidden_", i.getClass_model_hidden_());
            contentBuilder.append(str + "\n");
        }
        in.close();
        String content = contentBuilder.toString();
        System.out.println(content);
        return content;
    }

    public String getDateHtml(DateHtml i) throws FileNotFoundException, IOException {
        StringBuilder contentBuilder = new StringBuilder();
        InputStream stream = this.getClass().getResourceAsStream("/com/it/html/dataHtml.html");
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String str;
        while ((str = in.readLine()) != null) {
            str = str.replaceAll("_for_data_model_", i.getFor_data_model_());
            str = str.replaceAll("_name_data_model_", i.getName_data_model_());
            str = str.replaceAll("_for_model_hidden_", i.getFor_data_model_());
            str = str.replaceAll("_label_model_hidden_", i.getLabel_data_model_());
            contentBuilder.append(str + "\n");
        }
        in.close();
        String content = contentBuilder.toString();
        System.out.println(content);
        return content;
    }

    public String getCheckbox(CheckBox i) throws FileNotFoundException, IOException {
        StringBuilder contentBuilder = new StringBuilder();
        InputStream stream = this.getClass().getResourceAsStream("/com/it/html/checkboxModel.html");
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String str;
        while ((str = in.readLine()) != null) {
            str = str.replaceAll("_classCheckboxModel_", i.getClassCheckboxModel_());
            str = str.replaceAll("_classInputCheckBoxModel_", i.getClassInputCheckBoxModel_());
            str = str.replaceAll("_id_checkBox_", i.getId_checkBox_());
            str = str.replaceAll("_labelCheckboxModel_", i.getLabelCheckboxModel_());
            str = str.replaceAll("_name_Checkbox_", i.getName_Checkbox_());
            str = str.replaceAll("_value_checkbox_", i.getValue_checkbox_());
            contentBuilder.append(str + "\n");
        }
        in.close();
        String content = contentBuilder.toString();
        System.out.println(content);
        return content;
    }

//     ;
//     ;
//     ;
//     ;
//     ;
//     ;
//     ;
//
//    _name_radio_b_ ;
//    _value_radio_b_ ;
//    _for_radio_b_ ;
//    _error_name_radio_b_ ;
//    _desc_radio_b_ ;
// 
    public String getRadio(Radio i) throws FileNotFoundException, IOException {
        StringBuilder contentBuilder = new StringBuilder();
        InputStream stream = this.getClass().getResourceAsStream("/com/it/html/modelRadio.html");
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String str;
        while ((str = in.readLine()) != null) {
            str = str.replaceAll("_classModelRadio_", i.getClassModelRadio_());
            str = str.replaceAll("_labelModelRadio_", i.getLabelModelRadio_());

            str = str.replaceAll("_name_radio_a_", i.getId_radio_a_());
            str = str.replaceAll("_value_radio_a_", i.getValue_radio_a_());
            str = str.replaceAll("_for_radio_a_", i.getFor_radio_a_());
            str = str.replaceAll("_id_radio_a_", i.getId_radio_a_());
            str = str.replaceAll("_error_name_radio_a_", i.getError_name_radio_a_());
            str = str.replaceAll("_desc_radio_a_", i.getDesc_radio_a_());

            str = str.replaceAll("_name_radio_b_", i.getName_radio_b_());
            str = str.replaceAll("_value_radio_b_", i.getValue_radio_b_());
            str = str.replaceAll("_for_radio_b_", i.getFor_radio_b_());
            str = str.replaceAll("_id_radio_b_", i.getId_radio_b_());
            str = str.replaceAll("_error_name_radio_b_", i.getError_name_radio_b_());
            str = str.replaceAll("_desc_radio_b_", i.getDesc_radio_b_());
            contentBuilder.append(str + "\n");
        }
        in.close();
        String content = contentBuilder.toString();
        System.out.println(content);
        return content;
    }
 
    public String getSelect(Select i) throws FileNotFoundException, IOException {
        StringBuilder contentBuilder = new StringBuilder();
        InputStream stream = this.getClass().getResourceAsStream("/com/it/html/modelRadio.html");
        BufferedReader in = new BufferedReader(new InputStreamReader(stream));
        String str;
        while ((str = in.readLine()) != null) {
            str = str.replaceAll("_class_select_", i.getClass_select_());
            str = str.replaceAll("_name_select_", i.getName_select_());
            str = str.replaceAll("_id_select_", i.getId_select_());
            str = str.replaceAll("_classSelectModel_", i.getClassSelectModel_());
            str = str.replaceAll("_label_Select_model_", i.getLabel_Select_model_());

            contentBuilder.append(str + "\n");
        }
        in.close();
        String content = contentBuilder.toString();
        System.out.println(content);
        return content;
    }
}
